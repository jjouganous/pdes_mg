#include <cadmos.h>
#include <unistd.h>
#include <algorithm>
#include <vector>

#include "model.h"

// void calc_derivatives(const MeshDesc&, const std::vector<field>&, const std::vector<field>&, Modes_t&);
void checkArgs(int&, char **&, std::string&);

int main(int argc, char ** argv) {
	Cadmos::WarmUp(argc, argv);


    std::string CPinit=""; checkArgs(argc,argv, CPinit);

    // Lecture des paramètres
    Params pp=Params::load("./params.txt");

    std::vector<field> P0;
    
    field Pdata;
    
    MeshDesc desc;
    auto vectT=pp.array("Tsnap"); // instants des snapshots à sauvegarder
    if(CPinit!="") {
        auto my_pair=MeshDesc::from_file(CPinit);
        desc=my_pair.first;
        Pdata=my_pair.second;

    }
    else {
        desc=MeshDesc(pp["Nx"],pp["Ny"]);

        auto init_fn=[&desc] (double x, double y) -> double {
                const double rx=x-desc.d1*desc.N1/2.0; const double ry=y-desc.d2*desc.N2/2.0;
                return exp(-(pow(rx,2.0)+pow(ry,2.0))/(50.0*pow(0.01,2.0)));
        };
        Pdata=desc.field_from_fn(init_fn);
    }
    /*
    // ******************
    // Avec 4 populations
    // ******************
    field f=desc.new_field(); f.setConstant(0.0);
    field g=desc.new_field(); g.setConstant(0.0);
    
    for (int i=0;i<desc.N1;++i){
        for (int j=0;j<desc.N2;++j){
            if (i>desc.N1/2){
                f(i,j)=1.0;
            }
            if (j>desc.N2/2){
                g(i,j)=1.0;
            }
        }
    }
    
    field f1=0.25*f*g*Pdata;
    field f2=0.25*(1.0-f)*g*Pdata;
    field f3=0.25*f*(1.0-g)*Pdata;
    field f4=0.25*(1.0-f)*(1.0-g)*Pdata;
    P0.push_back(f1);
    P0.push_back(f2);
    P0.push_back(f3);
    P0.push_back(f4);*/
    
    //**************************
    // apparition d'une mutation
    //**************************
    field h=desc.new_field(); h.setConstant(0.0);
    for (int i=0;i<desc.N1;++i){
        for (int j=0;j<desc.N2;++j){
            if (pow(i-desc.N1/2,2)+pow(j-desc.N2/2,2)<16){
                h(i,j)=Pdata(i,j);
            }
        }
    }
    Pdata=Pdata-h;
    P0.push_back(Pdata);
    P0.push_back(h);
    
    Meta mod(desc, pp);
    mod.integrate(vectT,P0);

	Cadmos::CoolDown();
}



// ============================
// = Traitement des arguments =
// ============================
void checkArgs(int& argc, char **& argv, std::string& CPinit) {
    int ch;

    while ((ch = getopt(argc, argv, "c:p:m:")) != -1)
        switch (ch) {
            case 'c':
                CPinit=optarg;
                break;
        }

    argc -= optind;
    argv += optind;
}

