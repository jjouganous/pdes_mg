 #include "model.h"

Meta::Meta(const MeshDesc& desc, Params pp, std::string rootDir):_geom(desc), _params(pp),_root(rootDir) {
   alloc();
    Vx.setConstant(0.0);  Vy.setConstant(0.0);
    K.setConstant(1.0);
    _etslw=-1.0;
    _outDir=_root+"/res-"+std::to_string(_params.hash());
    _ww=std::unique_ptr<Writer>(new Writer(_geom, _outDir));
    pp.save((_outDir+"/params.txt").c_str());
}


std::string Meta::integrate(std::vector<double> Tsnap, const std::vector<field>& Pv0) {
    double t=0.0;
    int nb_snap=Tsnap.size(); // nombre de snapshots
    double Tmax=Tsnap[nb_snap-1]; // temps final de simulation
    std::vector<double> mass;
    std::vector<double> vol;
    std::vector<double> tps;
    std::vector<double> tps_snapM;
    std::vector<double> err_L2_snap;
    
    // Paramètres
    const double M0=_params["M0"];
    const double M1=_params["M1"];
    const double eta=_params["eta"];
    const double Mcr=_params["Mcr"];
    double delta=_params["delta"];

    auto gamma=_params.array("gamma");
    auto Mth=_params.array("Mth");
    
    double tps_s[P.size()];
    P=Pv0;
    S=(1.0-CP);
    M.setConstant(M0);
    CP.setConstant(0.0);
    for (int i=0;i<P.size();++i){
        CP=CP+P[i];
    }
    
    field f=_geom.new_field(); f.setConstant(0.0);
    for (int i=0;i<P.size();++i){
        G.push_back(f);
    }
    
    double mass_init=(CP*_geom.mesh_vol()).sum();
    
    SolvDiff diff(_geom, true); SolvTransp transp(_geom);

    auto rhs=_geom.new_field(); auto divV=_geom.new_field();

    double dt=1e-2;
    tps.push_back(t);
    mass.push_back(mass_init);
    vol.push_back((CP/(CP+0.00001)*_geom.mesh_vol()).sum());
    save2disk(t); // sauvegarde à l'instant initial

    int ctr_snap=0; // compteur de snapshot
    bool ind_reg=false; // indicateur d'enregistrement de snapshot
    bool ind_cr=false; // indicateur permettant d'arrêter la simu si la taille critique est atteinte
    double tc=Tmax;
	double errc[P.size()+1];

    while ((t < Tmax)&&(ind_cr==false)) {
        rhs=-divV;
        // Calcul de la pression
        diff.solvePoisson_Dirichlet(Pi, rhs, K, 0.0);

        // Calcul de la vitesse
        field Vxo(Vx), Vyo(Vy);
        calcDarcy(Pi, K, Vx, Vy);
        //calcGamma(GP,M,Mth[0],gamma[0]);
        for (int i=0;i<P.size();++i){
            calcGamma(G[i],M,Mth[i],gamma[i]);
        }
        dt=std::min(std::min(transp.calc_tstep(Vx,Vy,0.45),1e-2),1.0/eta);

        if ((t+dt>Tsnap[ctr_snap])||(t+dt==Tmax)){
            dt=Tsnap[ctr_snap]-t;
            ctr_snap++;
            ind_reg=true;
        }
        t+=dt;
       // Calcul de la divergence de la vitesse
        //calcDivV( CP, M, t, divV);
        //calcDivV( CP, GP, t, divV);
        divV.setConstant(0.0);
        for (int i=0;i<P.size();++i){
            divV=divV+G[i]*P[i];
        }
        divV=divV-delta*fth(t)*CP;
        
        // Transport des cellules
        //transp.solveConserv(CP, Vx, Vy, divV, dt);
        
        // Transport + Mitose + Mort + thérapie
        //CP*=(dt*(M-delta*fth(t))).exp();
        //CP*=(dt*(GP-delta*fth(t))).exp();
        for (int i=0;i<P.size();++i){
            transp.solveConserv(P[i], Vx, Vy, divV, dt);
            P[i]*=(dt*(G[i]-delta*fth(t))).exp();
        }
        // Quantité totale de cellules tumorales
        CP.setConstant(0.0);
        for (int i=0;i<P.size();++i){
            CP=CP+P[i];
        }
        //tissu sain
        S=1.0-CP;

        // Vascularisation
        M=M1+(M-M1)*(-eta*CP*dt).exp();

        tps.push_back(t);
        mass.push_back((CP*_geom.mesh_vol()).sum());

        double volume=0.0;
        for (int i=0;i<CP.size();i++) {
            if (CP(i)>=0.1){
                volume++;
            }
        }
        volume*=_geom.mesh_vol();
        vol.push_back(volume);
        

// Sauvegarde des simus ...
        if (ind_reg){
            save2disk(t);
            ind_reg=false;
        }
    } // fin de la boucle en temps
    
    
//test d'acceptation du temps critique
    double errf=fabs((CP*_geom.mesh_vol()).sum()- Mcr)/Mcr;
    if ((float)Mcr==0.0){
        tc=-1;
    }
    else if ((errf>0.03)){ // on s'accorde 3% d'erreur
        tc=-1;
        std::cout<<"taille critique non atteinte"<<std::endl;
    }

	for(int i=0;i<P.size();i++){
	    tps_snapM.push_back(tps_s[i]);
    }
    
    tps_snapM.push_back(tc);

	for(int i=0;i<P.size();i++){
	    err_L2_snap.push_back(errc[i]);
    }

//écriture des champs scalaires et de la tumeur critique
    if(_ww != NULL) {
        _ww->writeStdVecAscii("Masse", mass);
        _ww->writeStdVecAscii("Temps", tps);
        _ww->writeStdVecAscii("Volume", vol);
        _ww->writeStdVecAscii("Tps_snapM", tps_snapM);
		_ww->writeStdVecAscii("Err_L2", err_L2_snap);
    }
    return _outDir;
}


void Meta::alloc() {
    CP=_geom.new_field(); K=_geom.new_field();
    S=_geom.new_field(); Pi=_geom.new_field();
    Vx=_geom.new_field(); Vy=_geom.new_field();
    M=_geom.new_field(); GP=_geom.new_field();
}


void Meta::calcDarcy(const field& pi, const field& K, field& V1, field& V2) {
    Vx=-K*_geom.diff(pi,0); Vy=-K*_geom.diff(pi,1);
}

void Meta::calcGamma(field& G, const field& MVD, double seuil, double gamma0) {
    const double raideur=5.0;
    G=gamma0*(ctanh(raideur*(MVD-(seuil)))+1.0)/2.0;
}

/****************************
 * Calcul de la divergence
 ****************************/
void Meta::calcDivV( const field& CP, const field& M, double t, field& divV) {
    const double delta=_params["delta"];
    divV=(M-delta*fth(t))*CP;
}

double Meta::fth(double t) const {
    //if ((t>=0)&&(t<=7.4)) return 1.0;
    //else return 0.0;
    return 0.0;
}

void Meta::save2disk(double t) {
    if(_ww != NULL) {
//         std::cout << "• Masse = " << (CP*_geom.mesh_vol()).sum() << std::endl;
        //std::cout << "• Temps = " << t << std::endl;
        std::cout << "• Masse = " << (CP*_geom.mesh_vol()).sum() << std::endl;
        std::cout << "• Masse 1= " << (P[0]*_geom.mesh_vol()).sum() << std::endl;
        _ww->writeFieldVtk("CP", CP);
        for (int i=0;i<P.size();++i){
            _ww->writeFieldVtk("P"+std::to_string(i+1), P[i]);
        }
        for (int i=0;i<P.size();++i){
            _ww->writeFieldVtk("GP"+std::to_string(i+1), G[i]);
        }
        _ww->writeFieldVtk("M", M);
    }
}


