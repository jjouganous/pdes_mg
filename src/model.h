#include <cadmos.h>
#include <memory>

class Meta {
public:
    Meta(const MeshDesc&, Params, std::string rootDir=".");
    ~Meta() = default;

    std::string integrate(std::vector<double>,const std::vector<field>&);

    inline std::string outDir() const { return _outDir;};

private:
    void alloc();
    void calcDarcy(const field&, const field&, field&, field&);
    void calcDivV(const field& CP, const field& M, double t, field& divV);
    void calcGamma(field&, const field&, double, double);
    double fth(double t) const; //cytotoxique
    void save2disk(double);
    // Variables passées au constructeur
    const MeshDesc& _geom;
    Params _params;
    std::string _root;

    // Ellapsed time since the last snapshot
    double  _etslw;

    // Variables d'instance
    field CP;
    //field CPMC;
    field K;
    field S;
    field Pi;
    field Vx, Vy;
    field M;
    field GP;
    std::vector<field> G;
    std::vector<field> P;

    // Pour l'écriture
    std::string _wr_dir;
    std::unique_ptr<Writer> _ww;
    std::string _outDir;
};
