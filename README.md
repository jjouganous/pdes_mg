Meta_Cadmos
===========

*Versions du modèle direct et du modèle réduit pour les métastases pulmonaire baties sur [Cadmos](https://bitbucket.org/osaut/cadmos).*

## 1. Compilation
Après avoir récupéré les sources : 

    cmake .
    make

Un exécutable `meta` est créé ainsi que l'interface pour ffi (`ffi/libffi_meta.dylib`).

## 2. Utilisation
### Modèle complet
Pour lancer le modèle complet

    ./meta -c P0.vtk -p Pi0.vtk

où `P0.vtk` est la donnée initiale (optionnelle) pour la densité de cellule et `Pi0.vtk` la pression initiale (optionnelle).

### Modèle réduit
Pour lancer le modèle réduit, si les modes POD sont dans le dossier `./modes_pod`

    ./meta -m ./modes_pod -c P0.vtk -p Pi0.vtk

## 3. Appel en Ruby
Pour appeler la résolution du problème direct ou réduit, on peut utiliser l'encapsulation faite dans le dossier `ffi`.

L'interface est définie dans le fichier `ffi_interface`, c'est le point d'entrée du script ruby. Le plus simple est de n'échanger que des structures simples entre Ruby et C++ comme des chaines (chemins vers des données, descritpion des paramètres) et des nombres. 

Un exemple de script ruby est donné dans `launch_model.rb`.
